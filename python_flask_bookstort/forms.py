#!/usr/bin/python
# -*- coding: UTF-8 -*-

from __future__ import unicode_literals
from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField, PasswordField
from wtforms.validators import DataRequired, Length


class InsertBookListForm(FlaskForm):
    book_name = StringField('图书名称', validators=[DataRequired(), Length(1, 64)])
    book_price = StringField('图书价格', validators=[DataRequired()] )
    submit = SubmitField('添加')


class ModifyBookListForm(FlaskForm):
    book_name = StringField('图书名称', validators=[DataRequired(), Length(1, 64)])
    book_price = StringField('图书价格', validators=[DataRequired()] )
    submit = SubmitField('修改')


class RegisterForm(FlaskForm):
    user_name = StringField('用户名', validators=[DataRequired(), Length(1, 30)])
    user_password = PasswordField('密码', validators=[DataRequired(), Length(1, 30)])
    submit = SubmitField('注册')